import Core
import HTTP
import Router
import Middleware

let router = Router { route in
    route.get("/") { _ in
        print(route)
        return Response(status: .OK, body: "Oi")
    }

    route.get("/users/:id") { request in
        let id = request.parameters["id"]
        print(id)
        return Response(status: .OK)
    }

    route.post("/users") { request in
        let leJson: JSON = ["message": "Yeah!"]
        return Response(status: .OK, json: leJson)
    }

    let staticFiles = FileResponder(basePath: "static/")
    route.fallback(staticFiles)

    // route.any("/.*") { request in
    //     print("Not found")
    //     return Response(status: .NotFound, body: "Not Found")
    // }

    route.fallback() {
        return Response(status: .NotFound, body: "Not Found")
    }
}
