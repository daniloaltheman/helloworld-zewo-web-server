/*
    HelloWorld by Danilo Altheman - 2016 Nenhum direito reservado!
    To compile: swift build
    To execute: .build/debug/HelloWorld
    Or try the sym link: ./HelloWorld
*/


#if os(Linux)
    import Glibc
#else
    import Darwin.C
#endif

import CHTTPParser
import CLibvenice
import Epoch

// Server(port: 8080, responder: router).startInBackground()
Server(port: 60000, responder: router).start()
