import PackageDescription

let package = Package(
	name: "HelloWorld",
	dependencies: [
        .Package(url: "https://github.com/Zewo/Epoch.git", majorVersion: 0, minor: 1),
		.Package(url: "https://github.com/Zewo/Middleware.git", majorVersion: 0, minor: 1),
		.Package(url: "https://github.com/Zewo/CLibvenice.git", majorVersion: 0, minor: 1),
		.Package(url: "https://github.com/Zewo/CHTTPParser.git", majorVersion: 0, minor: 1)
	]
)
